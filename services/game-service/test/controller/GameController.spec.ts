import "reflect-metadata"; // Irritating
import chai from "chai";
import {GameController} from "../../src/controller/GameController";
import { expect } from "chai";

var sinonChai = require("sinon-chai");
chai.use(sinonChai);

describe('GameController', async() => {
    it('returns all games', async() => {
        const gameController = new GameController();
        const actualCats = await gameController.getGames();
        expect(actualCats).to.be.deep.eq(['Bloodborne', 'Sekiro', 'Persona 5 Royal']);
    });
});