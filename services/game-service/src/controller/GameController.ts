import {controller, httpGet} from "inversify-express-utils";

@controller('/games')
export class GameController {

    @httpGet('/')
    public async getGames() : Promise<string[]> {
        return ['Bloodborne', 'Sekiro', 'Persona 5 Royal'];
    }

}