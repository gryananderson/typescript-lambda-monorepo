import {controller, httpGet} from "inversify-express-utils";

@controller('/cats')
export class CatController {

    @httpGet('/')
    public async getCats() : Promise<string[]> {
        return ['Tater', 'Frances'];
    }

}