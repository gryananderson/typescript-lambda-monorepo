import "reflect-metadata"; // Irritating
import chai from "chai";
import {CatController} from "../../src/controller/CatController";
import { expect } from "chai";

var sinonChai = require("sinon-chai");
chai.use(sinonChai);

describe('CatController', async() => {
    it('returns all cats', async() => {
        const catController = new CatController();
        const actualCats = await catController.getCats();
        expect(actualCats).to.be.deep.eq(['Tater', 'Frances']);
    });
});