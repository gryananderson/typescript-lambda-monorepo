### Build: `lerna run build`

### Package: `sam package`

### Tests: `lerna run test:unit`

### Run local Express servers: `lerna run start`
Issue w/ the above - Multiple express servers are started on different ports. Ctl+C leaves them running in the background, requiring an explicit kill. Need to clean this up.

#### Cat-service: `curl http://localhost:3000/cats`
#### Game-service: `curl http://localhost:3001/games`

### Run local SAM event
#### Cat-service: `sam local invoke CatServiceFunction -e services/cat-service/events/event.json`
#### Game-service: `sam local invoke CatServiceFunction -e services/game-service/events/event.json`

### Deploy: `sam deploy`

#### Cat-service: `curl --insecure https://vob5dtdxzc.execute-api.us-east-2.amazonaws.com/Prod/cats`
#### Game-service: `curl --insecure https://vob5dtdxzc.execute-api.us-east-2.amazonaws.com/Prod/games`
